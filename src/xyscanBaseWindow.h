//-----------------------------------------------------------------------------
//  Copyright (C) 2020 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//
//  Author: Thomas S. Ullrich
//  Last update: Sep 17, 2020
//-----------------------------------------------------------------------------
#ifndef xyscanBaseWindow_h
#define xyscanBaseWindow_h

#include <QMainWindow>
#include <QColor>
#include <QBarSet>
#include <QtCharts>

class QAction;
class QActionGroup;
class QCheckBox;
class QComboBox;
class QDockWidget;
class QDoubleSpinBox;
class QGraphicsView;
class QGraphicsScene;
class QGraphicsLineItem;
class QGraphicsPixmapItem;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QRadioButton;
class QToolBox;

class xyscanAbout;
class xyscanErrorBarScanModeToolBox;
class xyscanDataTable;
class xyscanHelpBrowser;
class xyscanGraphicsView;

class xyscanBaseWindow : public QMainWindow
{
    Q_OBJECT

public:
    xyscanBaseWindow();
    virtual ~xyscanBaseWindow();

protected:
    void createMenuActions();
    void createMenus();
    void createStatusBar();
    void createToolBox();
    void createAdjustmentToolBoxItem(QWidget*);
    void createAxisSettingsToolBoxItem(QWidget*);
    void createCoordinateToolBoxItem(QWidget*);
    void createMeasureToolBoxItem(QWidget*);
    void createTableDockingWidget();
    void createZoomDockingWidget();
    void createHistogramDockingWidget();
    void createMarker();
    void createCrosshair();
    void createHelpBrowser();
    void createToolTips();

protected:
    enum {mMaxRecentFiles = 5};
    enum {mMaxSignificantDigits = 16};
    enum {mPixelUnits, mPlotUnits, mUserUnits};
    enum {mXLower, mXUpper, mYLower, mYUpper};
    enum {mHorizontalAndVerticalHistogram=0, mHorizontalHistogram, mVerticalHistogram};

    QAction *mOpenAction;
    QAction *mSaveAction;
    QAction *mPrintAction;
    QAction *mFinishAction;
    QAction *mDeleteLastAction;
    QAction *mDeleteAllAction;
    QAction *mEditCommentAction;
    QAction *mEditCrosshairColorAction;
    QAction *mEditMarkerColorAction;
    QAction *mEditPathColorAction;
    QAction *mAboutAction;
    QAction *mHelpAction;
    QAction *mCheckForUpdatesAction;
    QAction *mSetLowerXAction;
    QAction *mSetUpperXAction;
    QAction *mSetLowerYAction;
    QAction *mSetUpperYAction;
    QAction *mPasteImageAction;
    QAction *mRecentFileAction[mMaxRecentFiles];
    QAction *mSignificantDigitsAction[mMaxSignificantDigits];
    QAction *mClearHistoryAction;
    QAction *mShowPrecisionAction;
    QAction *mScreenScanAction;
    QAction *mShowTooltipsAction;

    QActionGroup *mSignificantDigitsActionGroup;

    QBarSeries *mBarSeries;

    QCheckBox *mMeasureAngleRadiansCheckBox;
    QCheckBox *mMeasurePathCheckBox;

    QColor  mCrosshairColor;
    QColor  mMarkerColor;
    QColor  mPathColor;

    QComboBox *mHistogramModeComboBox;
    QComboBox *mMeasureScaleComboBox;

    QDockWidget *mTableDock;
    QDockWidget *mZoomDock;
    QDockWidget *mHistogramDock;

    QDoubleSpinBox *mAngleSpinBox;
    QDoubleSpinBox *mScaleSpinBox;
    QDoubleSpinBox *mZoomSpinBox;

    QGraphicsLineItem   *mMarker[4];
    QGraphicsLineItem   *mCrosshairH;
    QGraphicsLineItem   *mCrosshairV;
    QGraphicsLineItem   *mMeasureLine;
    QGraphicsPathItem   *mMeasurePathLine;
    QGraphicsPixmapItem *mPointMarker;
    QGraphicsScene      *mImageScene;
    QGraphicsView       *mZoomView;

    QLabel *mMessageLabel;
    QLabel *mXCoordinateLabel;
    QLabel *mYCoordinateLabel;
    QLabel *mMeasuredDistanceLabel;
    QLabel *mMeasureScaleValueLabel;
    QLabel *mPlotInfoLabel;

    QLineEdit *mUpperYValueField;
    QLineEdit *mLowerYValueField;
    QLineEdit *mUpperXValueField;
    QLineEdit *mLowerXValueField;

    QLineEdit *mPixelXDisplay;
    QLineEdit *mPixelYDisplay;
    QLineEdit *mPlotXDisplay;
    QLineEdit *mPlotYDisplay;

    QLineEdit *mMeasureDXDisplay;
    QLineEdit *mMeasureDYDisplay;
    QLineEdit *mMeasureDRDisplay;
    QLineEdit *mMeasureDADisplay;
    QLineEdit *mMeasurePathDisplay;

    QLineEdit *mMeasureUserScaleField;

    QMenu *mFileMenu;
    QMenu *mEditMenu;
    QMenu *mViewMenu;
    QMenu *mHelpMenu;
    QMenu *mRecentFilesMenu;
    QMenu *mSignificantDigitsMenu;

    QPushButton *mSetLowerXButton;
    QPushButton *mSetUpperXButton;
    QPushButton *mSetLowerYButton;
    QPushButton *mSetUpperYButton;

    QPushButton *mStartMeasureButton;
    QPushButton *mClearMeasuredPathButton;

    QRadioButton *mLogXRadioButton;
    QRadioButton *mLogYRadioButton;

    QToolBox *mToolBox;

    QWidget *mCentralWidget;
    QWidget *mPlotAdjustmentItem;
    QWidget *mAxisSettingsItem;
    QWidget *mErrorScanModeItem;
    QWidget *mCoordinatesItem;
    QWidget *mMeasureItem;

    xyscanAbout        *mAboutDialog;
    xyscanHelpBrowser  *mHelpBrowser;
    xyscanGraphicsView *mImageView;
    xyscanDataTable    *mDataTable;
    xyscanErrorBarScanModeToolBox *mErrorBarScanModeToolBox;

    double  mImageScale;
    double  mImageAngle;
    double  mZoomScale;

    unsigned int mSignificantDigits = 6;
    const int    mHistogramRange = 5;
};

#endif
