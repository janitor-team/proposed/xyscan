//-----------------------------------------------------------------------------
//  Copyright (C) 2020 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//
//  Author: Thomas S. Ullrich
//  Last update: March 9, 2020
//-----------------------------------------------------------------------------
#ifndef xyscanScanTasksHandler_h
#define xyscanScanTasksHandler_h

class xyscanWindow;
#include <QObject>
#include <QPointF>
#include <vector>
#include <iostream>

using namespace std;

class xyscanScanTasksHandler : public QObject
{

    Q_OBJECT

    typedef void (xyscanScanTasksHandler::*TaskFunc)();
public:
    xyscanScanTasksHandler (xyscanWindow*);

    bool performTask();

private:
    void recordedXYPoint();

    void prepareXErrorScan();
    void prepareYErrorScan();

    void recordedLowerXError();
    void recordedUpperXError();
    void recordedLowerYError();
    void recordedUpperYError();

    void cleanUpXErrorScan();
    void cleanUpYErrorScan();

    void updateDisplays();

    void disableButtons();
    void enableButtons();

     void finish();

private:
    xyscanWindow* mWindow;
    vector<TaskFunc> mTaskList;
    vector<QPointF> mPoints;
    int mTaskIndex;

    int mNx;
    int mNy;
    int mNxCounter;
    int mNyCounter;

    QPointF mCursorBaseHPosition;
    QPointF mCursorBaseVPosition;
};
#endif

