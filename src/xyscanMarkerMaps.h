//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2015 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: March 28, 2014
//-----------------------------------------------------------------------------
#ifndef xyscanMarkerMaps_h
#define xyscanMarkerMaps_h

static const char *marker_cross_xpm[] = {
    "21 21 2 1",
    "  c None",
    "g c #00BB00",
    "ggggggg       ggggggg",
    "g                   g",
    "g                   g",
    "g                   g",
    "g         g         g",
    "g         g         g",
    "g         g         g",
    "          g          ",
    "          g          ",
    "          g          ",
    "    ggggggggggggg    ",
    "          g          ",
    "          g          ",
    "          g          ",
    "g         g         g",
    "g         g         g",
    "g         g         g",
    "g                   g",
    "g                   g",
    "g                   g",
    "ggggggg       ggggggg",
};

#endif


