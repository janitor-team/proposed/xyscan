//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2015 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: April 20, 2015
//-----------------------------------------------------------------------------
#ifndef xyscanGraphicsView_h
#define xyscanGraphicsView_h

#include <QGraphicsView>
#include <QDragEnterEvent>
#include <QString>

class xyscanGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    xyscanGraphicsView(QWidget* = 0);
    void dragEnterEvent(QDragEnterEvent*);
    void dragMoveEvent(QDragMoveEvent*);
    void dropEvent(QDropEvent*);
    
signals:
    void fileDropped(QString filename);
};

#endif
