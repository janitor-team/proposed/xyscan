//-----------------------------------------------------------------------------
//  Copyright (C) 2002-2015 Thomas S. Ullrich
//
//  This file is part of "xyscan".
//
//  This file may be used under the terms of the GNU General Public License.
//  This project is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License.
//  
//  Author: Thomas S. Ullrich
//  Last update: March 28, 2014
//-----------------------------------------------------------------------------
#ifndef xyscanAbout_h
#define xyscanAbout_h

#include <QDialog>
#include <QPixmap>
#include <QString>

class QFrame;
class QPushButton;
class QPushButton;
class QLabel;
class QHBoxLayout;
class QVBoxLayout;

class xyscanAbout : public QDialog
{
    Q_OBJECT
    
public:
    xyscanAbout(QWidget* parent = 0);

public slots:
    void accept();
    void toggleTitle();

private:
    QFrame*      mLine;
    QPushButton* mButtonLicense;
    QPushButton* mButtonOk;
    QLabel*      mTitlePixmapLabel;
    QLabel*      mTitleLicenseLabel;
    QHBoxLayout* mButtonLayout;
    QVBoxLayout* mLayout;
    QPixmap      mTitlePixmap;
    QString      mVersionText;
    bool         mTitlePixmapShown;
};

#endif


