#-----------------------------------------------------------------------------
#  Copyright (C) 2002-2020 Thomas S. Ullrich
#
#  This file is part of "xyscan".
#
#  This file may be used under the terms of the GNU General Public License.
#  This project is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License.
#  
#  Author: Thomas S. Ullrich
#  Last update: Feb 6, 2020
#-----------------------------------------------------------------------------
#
#  Unix/Linux syntax:
#  qmake [PREFIX=<dir>] xyscan.pro
#  where PREFIX is the directory under which xyscan will
#  be installed. The default (if PREFIX is not defined)
#  is /usr/local.
#
#  For Windows and macOS binary versions are available.
#  They should be used since building and deploying xyscan
#  on these platforms is not for the faint hearted.
#-----------------------------------------------------------------------------
VERSION = 4.6.2
TEMPLATE = app
CONFIG  += qt warn_on release c++14 lrelease
CONFIG  += sdk_no_version_check
QT += network xml widgets printsupport multimedia charts
QMAKE_APPLE_DEVICE_ARCHS = x86_64 arm64

macx {
QT += pdf
#  INCLUDEPATH += /usr/local/include/poppler/qt6/
#  INCLUDEPATH += /usr/local/include/poppler
#  LIBS += -L/usr/local/lib -lpoppler -lpoppler-qt6
}
RESOURCES += xyscan.qrc

isEmpty(PREFIX) {
    PREFIX = /usr/local
}

win32 {
   RC_FILE += platforms/win/xyscan.rc
   ICON += platforms/win/xyscan.ico
}

macx {
   ICON += platforms/macx/BundleItems/xyscan.icns
}

#
# For various lib location of poppler. If your installation is
# different you will have to add your location here.
#
unix {
    !macx {
        INCLUDEPATH += /usr/include/poppler/qt5/
        INCLUDEPATH += /usr/include/poppler
        INCLUDEPATH += /usr/local/include/poppler/qt5/
        INCLUDEPATH += /usr/local/include/poppler
        INCLUDEPATH += /opt/local/include/poppler/qt5/
        INCLUDEPATH += /opt/local/include/poppler
        LIBS += -L/usr/local/lib/ -L/usr/lib/ -lpoppler -lpoppler-qt5
    }
}

#
# For installation (make install)
#
unix {
   documentation.path = $$PREFIX/share/xyscan/docs
   documentation.files = docs/* license.txt gpl.txt
   target.path = $$PREFIX/bin/
   INSTALLS += documentation
   INSTALLS += target
}

#
# For macOS bundle
#
macx {
    xydocs.files = docs
    xydocs.path = Contents/Resources/
    xylicense.files = platforms/gpl.txt platforms/license.txt
    xylicense.path = Contents/
    xyicns.file = platforms/macx/BundleItems/xyscan.icns
    xyicns.path = Contents/Resources/
    xyfrench.files = platforms/macx/BundleItems/fr.lproj
    xyfrench.path = Contents/Resources/
    xydeutsch.files = platforms/macx/BundleItems/de.lproj
    xydeutsch.path = Contents/Resources/
    xyenglish.files = platforms/macx/BundleItems/en.lproj
    xyenglish.path = Contents/Resources/
    xyinfo.files = platforms/macx/BundleItems/Info.plist
    xyinfo.path = Contents/
    QMAKE_BUNDLE_DATA += xydocs xylicense xyicns xyfrench xyenglish xydeutsch xyinfo
}
                              
#
# All source files
#
HEADERS = ./src/xyscanWindow.h \
          ./src/xyscanAbout.h \
          ./src/xyscanVersion.h \
          ./src/xyscanHelpBrowser.h \
          ./src/xyscanMarkerMaps.h \
           ./src/xyscanUpdater.h \
          ./src/xyscanGraphicsView.h \
          ./src/xyscanDataTable.h \
          ./src/xyscanErrorBarScanModeToolBox.h \
          ./src/xyscanScanTasksHandler.h \
          ./src/xyscanBaseWindow.h

SOURCES = ./src/xyscanMain.cpp \
          ./src/xyscanWindow.cpp \
          ./src/xyscanAbout.cpp \
          ./src/xyscanHelpBrowser.cpp \
          ./src/xyscanUpdater.cpp \
          ./src/xyscanGraphicsView.cpp \
          ./src/xyscanDataTable.cpp \
          ./src/xyscanErrorBarScanModeToolBox.cpp \
          ./src/xyscanScanTasksHandler.cpp \
          ./src/xyscanBaseWindow.cpp

TRANSLATIONS = ./translations/xyscan_fr.ts \
               ./translations/xyscan_de.ts

DISTFILES += \
    ChangeLog.txt

